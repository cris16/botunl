/* global ID_TELEGRAM */

const TelegramBot = require('node-telegram-bot-api');
const boot = require('../bot/boot');
const telegram = require('./telegram');

module.exports = {
    coneccionTelegram
};

function coneccionTelegram(token, idTelegram) {
    // replace the value below with the Telegram token you receive from @BotFather
    console.log("token", token);
    var token = token;

// Create a bot that uses 'polling' to fetch new updates

    var bot = new TelegramBot(token, {polling: true});

// Matches "/echo [whatever]"
    bot.onText(/\/echo (.+)/, (msg, match) => {
        // 'msg' is the received Message from Telegram
        // 'match' is the result of executing the regexp above on the text content
        // of the message

        const chatId = msg.chat.id;
        const resp = match[1]; // the captured "whatever"

        // send back the matched "whatever" to the chat
        bot.sendMessage(chatId, resp);
    });

// Listen for any kind of message. There are different kinds of
// messages.
    bot.on('message', (msg) => {
        const chatId = msg.chat.id;
        var mensajeMultiple = [];
        boot.validarMensaje(idTelegram, chatId, msg['text'], ID_TELEGRAM, mensajeMultiple, JSON.stringify(msg.from), function (respuesta) {
            telegram.enviarMensaje(bot, chatId, mensajeMultiple);
            console.log("respuesta->",msg);
//                return facebook.enviarMensajeMultiples(messagingEvent['recipient']['id'], messagingEvent['sender']['id'], mensajeMultiple, 0, respuesta['logs']);
        });

//        console.log("Received your message", msg);
        // send a message to the chat acknowledging receipt of their message
//        bot.sendMessage(chatId, 'Received your message');
//    bot.sendMessage(chatId, "Welcome", {
//        "reply_markup": {
//            "keyboard": [["Sample text", "Second sample"], ["Keyboard"], ["I'm robot"]]
//        }
//    });
    });
    return bot;
}
