/* global MENSAJE */

var administrador = require('../logs/administradorLogs');

module.exports = {
    mensajeSimple,
    mensajeFormato,
    buscarTexto,
    mensajesMultiples,
    cargarAyuda
};

function mensajeSimple(mensaje) {
    return {text: mensaje};
}

function mensajesMultiples(data) {
    let dataMensaje = {};
    if (data['text'])
        dataMensaje['text'] = data['text'];
    if (data['quick_replies']) {
        dataMensaje['reply_markup'] = {keyboard: []};
        for (var i = 0; i < data['quick_replies'].length; i++) {
            dataMensaje['reply_markup']['keyboard'].push([data['quick_replies'][i]['payload']]);
        }
    }

    return dataMensaje;
}

function mensajeFormato(datosMensaje) {
    var data = datosMensaje['data'];
//    console.log("data", data, datosMensaje);
    var respuestaDialogFlow = datosMensaje['respuestaDialogFlow'];
    let mensaje = {};
    try {
        mensaje = data[0]['respuesta'];
        if (mensaje['text'])
            mensaje['text'] = respuestaDialogFlow['respusta'];
        if (mensaje['attachment'] && mensaje['attachment']['payload'] && mensaje['attachment']['payload']['text'])
            mensaje['attachment']['payload']['text'] = respuestaDialogFlow['respusta'];
    } catch (e) {

    }
    return mensaje;
}

function buscarTexto(messagingEvent) {
    var respuesta = "dfgfdgrergdfgfhhdrsdfd";//Bote el mensaje de error
    if (messagingEvent.message.hasOwnProperty('text')) {
        if (messagingEvent.message.hasOwnProperty('quick_reply'))
            respuesta = messagingEvent.message.quick_reply.payload;
        else
            respuesta = messagingEvent.message.text;
    } else if (messagingEvent.message.attachments[0]['payload']['coordinates']) {
        respuesta = JSON.stringify(messagingEvent.message.attachments[0]['payload']['coordinates']);
    } else {
        administrador.error("ERROR, NO SE DETECTO ELEMENTO, POR DEFECTO SE LE ENVIA COMO ERROR:", JSON.stringify(messagingEvent));
    }
    return respuesta;
}

function cargarAyuda(respuestas) {
    let dataMensaje = {text: "Alternativas de consulta"};
    dataMensaje['reply_markup'] = {keyboard: []};
    for (var i = 0; i < respuestas.length && i < 10; i++) {
        var data = respuestas[i];
        dataMensaje['reply_markup']['keyboard'].push([data['ayuda']]);
    }
    return dataMensaje;
//        quick_replies.push({content_type: "text", title: data['ayuda'], payload: data['ayuda']});
//    return {text: 'Escoja una de las siguientes opciones.', quick_replies};
}