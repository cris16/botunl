module.exports = {
    enviarMensaje
};

function enviarMensaje(bot, chatId, listaMensaje) {
    for (var i = 0; i < listaMensaje.length; i++) {
        if (listaMensaje[i]['reply_markup'] && listaMensaje[i]['text']){
            bot.sendMessage(chatId, listaMensaje[i]['text'], {reply_markup: listaMensaje[i]['reply_markup']});
        }else if (listaMensaje[i]['text'])
            bot.sendMessage(chatId, listaMensaje[i]['text']);
    }
}