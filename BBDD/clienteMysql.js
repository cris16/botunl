
/* global process, IS_DESARROLLO */

const mysql = require('mysql2');
const administrador = require('../logs/administradorLogs');

const MYSQL_STRING_DB = {
    host: process.env.MSQL_HOST ? process.env.MSQL_HOST : '',
    user: process.env.MSQL_USER ? process.env.MSQL_USER : '',
    password: process.env.MSQL_PASSWORD ? process.env.MSQL_PASSWORD : '',
    port: 3306,
    multipleStatements: false};


module.exports = {
    on,
    ejecutarSQL,
    ejecutarSQLCallback
};

function on(callback) {
    MYSQL_PING = mysql.createConnection(MYSQL_STRING_DB);
    MYSQL_PING.connect(function (err) {
        if (err) {
            administrador.error('config.js NO SE PUDO CONECTAR A MYSQL ' + err);
            callback(false);
        } else {
            administrador.log('Exito en conexion a MYSQL');
        }
    });
    MYSQL_PING.query('SELECT 1 + 1 AS solution', function (err) {
        if (err) {
            administrador.error('config.js NO SE PUDO COMPROBAR CONEXION A MYSQL ' + err);
            callback(false);
        } else {
            administrador.log('Exito al comprobar conexion a MYSQL');
            callback(true);
        }
    });
    MYSQL_PING.end();
}

var pool = mysql.createPool({
    connectionLimit: 50,
    host: MYSQL_STRING_DB.host,
    user: MYSQL_STRING_DB.user,
    password: MYSQL_STRING_DB.password,
    multipleStatements: false,
    charset: 'utf8mb4'
});

function ejecutarSQL(QUERY, VALORES) {
    pool.query(QUERY, VALORES, function (err) {
        if (err)
            administrador.error(err + ' SQL: ' + QUERY + ' VALORES: ' + VALORES);
    });
}
function ejecutarSQLCallback(QUERY, VALORES, callback) {
//    console.log("VALORES", VALORES,QUERY);
    pool.query(QUERY, VALORES, function (err, ejecucion) {
        if (err) {
            administrador.error(err + ' SQL: ' + QUERY + ' VALORES: ' + VALORES);
            if (IS_DESARROLLO)
                return callback({error: err.sqlMessage});
            return callback({error: 'err'});
        } else
            callback(ejecucion);
    });
}