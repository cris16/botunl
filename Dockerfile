
# Utiliza una imagen base de Node.js
FROM node:16

# Copia los archivos de package.json y package-lock.json al directorio de trabajo
COPY package*.json ./

# Instala las dependencias del proyecto
RUN npm install

# Copia el resto de los archivos del proyecto al directorio de trabajo
COPY . .

# Expone los puertos que deseas
EXPOSE 9089

# Comando para iniciar la aplicación llamando directamente al archivo servidor.js
CMD [ "node", "servidor.js" ]



