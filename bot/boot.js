/* global MENSAJE, modelo, MODELOCORPUS */

const modeloPreEntrenado = require('../entrenarModelo');
const config = require('../BBDD/clienteMysql');
var entidad1 = require('./entidades');
const recursos = require('../funciones/recursos');
const formatoMensaje = require('../funciones/formatoMensajes');
const guardarLog = require('../logs/respuestasObtenidas');
entidad1.cargarEntidades();
entidad1.cargarListaEntdades();

module.exports = {
    validarMensaje
};

function validarMensaje(recipienteId, sessionId, chat, aplicativo, mensajeMultiple, infoUser, callback) {
    var logs = {mensajeDailogflow: chat};
    var resIntencion = 'i_error';
//    console.log("chat1",chat);
    chat = chat.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    if (!mensajeMultiple)
        mensajeMultiple = [];
    var modeloEntidad = modeloPreEntrenado.realizarPrueba(chat, MODELOCORPUS);
    var inteciones = modeloEntidad['result'][0] ? modeloEntidad['result'][0] : null;
    console.log("chat", chat, inteciones);
    if (inteciones && inteciones['aproximado'] < 0.6) {
        inteciones['intens'] = 'i_error';
//        return callback({en: 1, intens: resIntencion, mensajeMultiple, logs});
//    else {
    }
    if (inteciones) {
        buscarIntencion(recipienteId, sessionId, aplicativo, inteciones, chat, mensajeMultiple, infoUser, function (resIntencion) {
            return callback(resIntencion);
        });
    } else
        return callback({en: -1, mensajeMultiple, logs});
//    }

}

function buscarIntencion(recipienteId, sessionId, aplicativo, intencion, chat, mensajeMultiple, infoUser, callback) {
    config.ejecutarSQLCallback(SQL_VER_RESPUSTA_INTENCION, [intencion['intens']], function (respuestas) {
        if (respuestas['error'])
            return callback({en: -1, mensaje: MENSAJE.get('INC'), respuestas: []});
        else if (respuestas.length <= 0)
            return callback({en: -1, mensaje: MENSAJE.get('NRDF'), respuestas});
        else {
            var data = respuestas[0];
            intencion['en'] = 1;
            intencion['intencionTipo'] = data['idIntencionTipo'];
            intencion['parametro'] = data['parametro'];
            intencion['mensaje'] = data['text'];
            intencion['respuestas'] = respuestas;

            if (data['parametro'] == 1)
            {
                intencion['entidades2'] = entidad1.buscarEntidad(chat);
//                intencion['entidades'] = entidad1.buscarListaEntidades(chat);
                recursos.buscarInformacion(intencion['entidades2'], intencion, mensajeMultiple, aplicativo, function (resBuscar) {
                    intencion['mmm'] = resBuscar;
                    intencion['mensajeMultiple'] = mensajeMultiple;
                    guardarLog.guardarLog(recipienteId, sessionId, chat, JSON.stringify(mensajeMultiple), intencion['aproximado'], data['idIntenciones'], infoUser);
                    return callback(intencion);

                });
            } else {
                intencion['entidades'] = {};
                for (var i = 0; i < respuestas.length; i++) {
                    mensajeMultiple.push(formatoMensaje.mensajesMultiples(respuestas[i]['respuesta'], aplicativo));
                }
//                mensajeMultiple.push(JSON.parse(respuestas[0]['respuesta']));
                guardarLog.guardarLog(recipienteId, sessionId, chat, JSON.stringify(mensajeMultiple), intencion['aproximado'], data['idIntenciones'], infoUser);
//                mensajeMultiple.push({"text": data['text']});
                return callback(intencion);
            }
        }
    });
}

const SQL_VER_RESPUSTA_INTENCION = "SELECT i.idIntenciones, i.text, i.idIntencionTipo, i.parametro, i.respuesta  "//,i.multipleMensajes, i.responder
        + "FROM botUnl.intenciones i"
//        + " LEFT JOIN " + BASE_LOGICA + ".parametro p ON p.idParametro=i.idParametro AND p.habilitado=1"
        + " WHERE i.nombre = ? AND i.habilitado = 1  ";


function obtnerEntidades(text, callback) {
    text = removeAccents(text).toLowerCase();
    var entidades = {};
    config.ejecutarSQLCallback(SQL_OBTENER_ENTIDADES, [], function (respuestas) {
        if (!respuestas['error'] && respuestas.length > 0) {
            for (var i = 0; i < respuestas.length; i++) {
                var data = respuestas[i];
                var pos = -1;
                pos = text.search(removeAccents(data['nombre']).toString());
                console.log("pos", pos);
                if (pos >= 0) {
                    entidades[data['clase']] = data;
                }
            }
        }
        return callback(entidades);
    });
}

var SQL_OBTENER_ENTIDADES = 'SELECT idEntidad,lower(nombre) as nombre,nivel,concat("nivel",nivel) as clase'
        + ' FROM botUnl.entidad where habilitado=1'
        + ' union all '
        + ' SELECT e.idEntidad,lower(es.nombre) as nombre,nivel,concat("nivel",e.nivel) as clase FROM botUnl.entidadSinonimo es'
        + ' inner join botUnl.entidad e on e.idEntidad=es.idEntidad and e.habilitado=1'
        + ' where es.habilitado=1';
const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
};