const config = require('../BBDD/clienteMysql');
const fs = require('fs');
const modeloPreEntrenado = require('../entrenarModelo');

var entidad = null;
var listaEntidades = [];


module.exports = {
    cargarEntidades,
    buscarEntidad,
    cargarListaEntdades,
    buscarListaEntidades
};
function buscarEntidad(text) {
    var respuesta = {}, respuestaAux = [];
    var modeloEntidad = modeloPreEntrenado.realizarPrueba(text, entidad);
    var entidades2 = modeloEntidad['result'] ? modeloEntidad['result'].filter(ent => ent.aproximado >= 0.65) : [];
//    console.log("modeloEntidad", entidades2, listaEntidades);
    for (var i = 0; i < entidades2.length; i++) {
        var dataAux = listaEntidades.filter(ent => ent.idEntidad == entidades2[i]['intens']);
//        console.log("dataAux", entidades2[i]['intens'], dataAux);
        if (dataAux.length > 0) {
            var data = dataAux[0];
            data['aproximado'] = entidades2[i]['aproximado'];
            respuestaAux.push(data);
//            respuesta['parametro' + (i+1)] = data;
        }
    }
    var respuestaAux1 = respuestaAux.sort((a, b) => (b.aproximado - a.aproximado));
    if (respuestaAux1.length > 3)
        respuestaAux1 = respuestaAux1.slice(0, 3);
    var respuestaAux2 = respuestaAux1.sort((a, b) => (a.nivel - b.nivel));
    let contador = 1;
    for (var i = 0; i < respuestaAux2.length; i++) {
        contador += 1;
        if (respuestaAux2[i]['nivel'] == 1)
            contador = 1;
//        console.log("contador", contador, respuestaAux2[i]['nivel']);
        respuesta['parametro' + (contador)] = respuestaAux2[i];
    }
    return respuesta;
}

async function cargarEntidades() {
    var cormpus = require('../corpora/defecto.json');
    cormpus['sentences'] = [];
    config.ejecutarSQLCallback(SQL_OBTENER_ENTIDADES, [], function (respuestas) {
        if (!respuestas['error'] && respuestas.length > 0) {
            for (var i = 0; i < respuestas.length; i++) {
                var data = respuestas[i];
                var info1 = data['nombre'];
                cormpus['sentences'].push({text: info1.normalize("NFD").replace(/[\u0300-\u036f]/g, ""), entities: [], intent: data['idEntidad']});
            }
            fs.writeFile('./corpora/dataEntidades.json', JSON.stringify(cormpus), (err) => {
                if (err)
                    throw err;
                else
                    entrenarModelo(cormpus);

                // success case, the file was saved
                console.log('Lyric entidad saved!');
            });
        }
    });
}
async function entrenarModelo(cormpus) {
//    modeloPreEntrenado.cargarCorpus('./corpora/dataEntidades.json');
    entidad = await modeloPreEntrenado.entrenarModelo('./corpora/dataEntidades.json', cormpus);
    console.log("entidad", entidad.outputLookup);
    console.log("modelo de entidades entrenado");
}

var SQL_OBTENER_ENTIDADES = 'SELECT idEntidad ,lower(nombre) as nombre,nivel,concat("nivel",nivel) as clase'
        + ' FROM botUnl.entidad where habilitado=1'
        + ' union all '
        + ' SELECT e.idEntidad,lower(es.nombre) as nombre,nivel,concat("nivel",e.nivel) as clase FROM botUnl.entidadSinonimo es'
        + ' inner join botUnl.entidad e on e.idEntidad=es.idEntidad and e.habilitado=1'
        + ' where es.habilitado=1';

function cargarListaEntdades() {
    config.ejecutarSQLCallback(SQL_OBTENER_ENTIDADES, [], function (respuestas) {
        if (!respuestas['error'] && respuestas.length > 0)
            listaEntidades = respuestas;

    });
}

function buscarListaEntidades(text) {
    var entidades = {};
    text = removeAccents(text).toLowerCase();
    for (var i = 0; i < listaEntidades.length; i++) {
        var data = listaEntidades[i];
        var pos = -1;
        pos = text.search(removeAccents(data['nombre']).toString());
//        console.log("pos",text, pos,data['nombre']);
        if (pos >= 0) {
            entidades[data['clase']] = data;
        }
    }
    return entidades;
}
const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
};