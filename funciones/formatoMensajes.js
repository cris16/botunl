/* global ID_FACEBOK */
var administrador = require('../logs/administradorLogs');
var facebook = require('../facebook/utils');
var telegram = require('../Telegram/utils');

module.exports = {
    mensajeSimple,
    mensajesMultiples,
    cargarAyuda
};


function mensajesMultiples(mensaje, aplicativo) {
    var respuesta = elegirAplicativo(mensaje, aplicativo, 'mensajesMultiples');
    if (respuesta['error']) {
        administrador.log(respuesta['error']);
        return {};
    } else
        return respuesta;
}

function mensajeSimple(mensaje, aplicativo) {
    let respuesta = elegirAplicativo(mensaje, aplicativo, 'mensajeSimple');
    if (respuesta['error']) {
        administrador.log(respuesta['error']);
        return {};
    } else
        return respuesta;
}

function cargarAyuda(listaData, aplicativo) {
    let respuesta = elegirAplicativo(listaData, aplicativo, 'cargarAyuda');
    if (respuesta['error']) {
        administrador.log(respuesta['error']);
        return {};
    } else
        return respuesta;
}

function elegirAplicativo(data, aplicativo, fun) {
    switch (parseInt(aplicativo)) {
        case ID_FACEBOK:
            if (facebook[fun])
                return  facebook[fun](data);
            else {
                return {error: "NO EXISTE LA FUNCIÓN"};
            }
            break;
        case ID_TELEGRAM:
            if (telegram[fun])
                return  telegram[fun](data);
            else {
                return {error: "NO EXISTE LA FUNCIÓN"};
            }
            break;
        default:
            return {error: "NO EXISTE LA CONFIGURACIÓN DEL APLICATIVO"};
    }
}
