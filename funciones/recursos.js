/* global MENSAJE, ID_FACEBOK */

const config = require('../BBDD/clienteMysql');
const formatoMensaje = require('../funciones/formatoMensajes');

module.exports = {
    buscarInformacion
};

function buscarInformacion(entidades, extra, listaMensaje, aplicativo, callback) {
//    console.log("listaMensaje");
    if (entidades['parametro1'] && entidades['parametro2'] && entidades['parametro3']) {
        var sql = "";
        if (entidades['parametro1']['nivel'] == 1) {
            var parametro = [
                entidades['parametro1']['idEntidad'], entidades['parametro2']['idEntidad'], entidades['parametro3']['idEntidad'],
                entidades['parametro1']['idEntidad'], entidades['parametro2']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro3']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro3']['idEntidad'],
                entidades['parametro3']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro1']['idEntidad'],
                entidades['parametro2']['idEntidad']
            ];
            ejecutarConsulta(SQL_CONSULTA_NIVEL1, parametro, listaMensaje, aplicativo, function (resConsulta) {
                if (listaMensaje.length > 0)
                    return callback(listaMensaje);
                else
                    ayudaPregunta(entidades, 1, listaMensaje, aplicativo, function () {
                        return callback({listaMensaje});
                    });
            });
        } else {
            var parametro = [
                entidades['parametro1']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro1']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro3']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro3']['idEntidad']
            ];
            ejecutarConsulta(SQL_CONSULTA_NIVEL2, parametro, listaMensaje, aplicativo, function (resConsulta) {
                if (listaMensaje.length > 0)
                    return callback(listaMensaje);
                else
                    ayudaPregunta(entidades, 2, listaMensaje, aplicativo, function () {
                        return callback({listaMensaje});
                    });
            })
        }

    } else if (entidades['parametro1'] && entidades['parametro2']) {
        if (entidades['parametro1']['nivel'] == 1) {
            var parametro = [
                entidades['parametro1']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro1']['idEntidad']
            ];
            ejecutarConsulta(SQL_CONSULTA_NIVEL1_2, parametro, listaMensaje, aplicativo, function (resConsulta) {
//                console.log("resConsulta",resConsulta);
                if (listaMensaje.length > 0)
                    return callback(listaMensaje);
                else
                    ayudaPregunta(entidades, 1, listaMensaje, aplicativo, function () {
                        return callback({listaMensaje});
                    });
            });
        } else {
            var parametro = [
                entidades['parametro1']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro2']['idEntidad'],
                entidades['parametro1']['idEntidad']
            ];
            ejecutarConsulta(SQL_CONSULTA_NIVEL2_2, parametro, listaMensaje, aplicativo, function (resConsulta) {
                if (listaMensaje.length > 0)
                    return callback(listaMensaje);
                else
                    ayudaPregunta(entidades, 3, listaMensaje, aplicativo, function () {
                        return callback({listaMensaje});
                    });
            });
        }
    } else if (entidades['parametro1']) {
        ayudaPregunta(entidades, 1, listaMensaje, aplicativo, function () {
            return callback({listaMensaje});
        });
    } else if (entidades['parametro2'] && entidades['parametro3']) {
        var parametro = [
            entidades['parametro2']['idEntidad'],
            entidades['parametro3']['idEntidad'],
            entidades['parametro3']['idEntidad'],
            entidades['parametro2']['idEntidad']
        ];
        config.ejecutarSQLCallback(SQL_CONSULTA_NIVEL2_3, parametro, function (respuestas) {
            if (!respuestas['error'] && respuestas[0]['total'] == 1)
                ejecutarConsulta(SQL_CONSULTA_NIVEL2_2, parametro, listaMensaje, aplicativo, function (resConsulta) {
                    if (listaMensaje.length > 0)
                        return callback(listaMensaje);
                    else
                        ayudaPregunta(entidades, 3, listaMensaje, aplicativo, function () {
                            return callback({listaMensaje});
                        });
                });
            else
                ayudaPregunta(entidades, 4, listaMensaje, aplicativo, function () {
                    return callback({listaMensaje});
                });
        });
    } else if (entidades['parametro2']) {
        ayudaPregunta(entidades, 5, listaMensaje, aplicativo, function () {
            return callback({listaMensaje});
        });
    } else {
        ayudaPregunta(entidades, 0, listaMensaje, aplicativo, function () {
            return callback({listaMensaje});
        });
    }
}

var SQL_CONSULTA_NIVEL1 = "(SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadUno=? and entidadDos=? and entidadTres=? limit 1) "
        + " union all (SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadUno=? and entidadDos=? limit 1)"
        + " union all (SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadUno=? and entidadDos=? limit 1)"
        + " union all (SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=? limit 1)"
        + " union all (SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=? limit 1)"
        + " union all (SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=? limit 1)";
var SQL_CONSULTA_NIVEL2 = "(SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where  entidadDos=? and entidadTres=? limit 1) "
        + " union all (SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=? limit 1)"
        + " union all (SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=? limit 1)";
var SQL_CONSULTA_NIVEL2_2 = "(SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=? limit 1) "
        + " union all SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=? limit 1";
var SQL_CONSULTA_NIVEL2_3 = "select count(*) as total from ((SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=? ) "
        + " union all (SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadDos=? and entidadTres=?)) as aux";
var SQL_CONSULTA_NIVEL1_2 = "(SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo FROM botUnl.relacion where entidadUno=? and entidadDos=? limit 1) ";

function ejecutarConsulta(consulta, parametro, listaMensaje, aplicativo, callback) {
    config.ejecutarSQLCallback(consulta, parametro, function (respuestas) {
        if (respuestas['error'])
            listaMensaje.push(formatoMensaje.mensajeSimple("Algo ocurrio, intene mas tarde", aplicativo));
        else if (respuestas.length > 0) {
            if (aplicativo == ID_FACEBOK && respuestas[0]['text'].length > 2000) {
                for (var i = 0; i < respuestas[0]['text'].length; ) {
                    listaMensaje.push(formatoMensaje.mensajeSimple(respuestas[0]['text'].substr(i, 2000), aplicativo));
                    i += 2000;
                }
            } else
                listaMensaje.push(formatoMensaje.mensajeSimple(respuestas[0]['text'], aplicativo));
        }
        return callback(listaMensaje);

    });
}

function ayudaPregunta(entidades, tipo, listaMensaje, aplicativo, callback) {
    var sql = SQL_OBTENER_AYUDA_0, parametros = [], mExtra = "";
    switch (tipo) {
        case 1:
            sql = SQL_OBTENER_AYUDA_1;
            parametros = [entidades['parametro1']['idEntidad'], entidades['parametro1']['idEntidad'], entidades['parametro1']['idEntidad']];
            mExtra += "\n" + entidades['parametro1']['nombre'];
            break;
        case 2:
            sql = SQL_OBTENER_AYUDA_1_2;
            parametros = [entidades['parametro1']['idEntidad'], entidades['parametro2']['idEntidad']];
            mExtra += "\n" + entidades['parametro1']['nombre'];
            break;
        case 3:
            sql = SQL_OBTENER_AYUDA_2_2;
            parametros = [entidades['parametro1']['idEntidad'], entidades['parametro2']['idEntidad']];
            mExtra += "\n" + entidades['parametro1']['nombre'];
            break;
        case 4:
            sql = SQL_OBTENER_AYUDA_2_2;
            parametros = [entidades['parametro2']['idEntidad'], entidades['parametro3']['idEntidad']];
            break;
        case 5:
            sql = SQL_OBTENER_AYUDA_2_3;
            parametros = [entidades['parametro2']['idEntidad'], entidades['parametro2']['idEntidad']];
            break;
        default:
            break;

    }
//    console.log("sql",sql,parametros,tipo);
    config.ejecutarSQLCallback(sql, parametros, function (respuestas) {
        if (respuestas['error'])
            listaMensaje.push(formatoMensaje.mensajeSimple(MENSAJE.get('UPS'), aplicativo));
        else if (respuestas.length <= 0)
            listaMensaje.push(formatoMensaje.mensajeSimple(MENSAJE.get('SRE'), aplicativo));
        else {
            listaMensaje.push(formatoMensaje.mensajeSimple(MENSAJE.get('SPE'), aplicativo));
            listaMensaje.push(formatoMensaje.cargarAyuda(respuestas, aplicativo));
        }
        return callback({listaMensaje});
    });
}

var SQL_OBTENER_AYUDA_0 = "SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo,ayuda FROM botUnl.relacion order by articulo limit 12";
var SQL_OBTENER_AYUDA_1 = "SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo,ayuda FROM botUnl.relacion where entidadUno=? || entidadDos=? || entidadTres=? order by entidadUno limit 12";
var SQL_OBTENER_AYUDA_1_2 = "SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo,ayuda FROM botUnl.relacion where entidadUno=? && entidadDos=? order by entidadDos limit 12";
var SQL_OBTENER_AYUDA_2_2 = "SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo,ayuda FROM botUnl.relacion where entidadDos=? || entidadTres=? order by entidadTres limit 12";
var SQL_OBTENER_AYUDA_2_3 = "SELECT entidadUno, entidadDos, entidadTres, orden, text, articulo,ayuda FROM botUnl.relacion where entidadDos=? || entidadTres=? order by entidadDos limit 12";

function cargarAyuda(respuestas) {
    var quick_replies = [], buttons = [], elements = [];
    for (var i = 0; i < respuestas.length; i++) {
        var data = respuestas[i];
        if (buttons.length == 3) {
            elements.push({title: "Opciones de pregunta.", buttons});
            buttons = [];
        }
        buttons.push({type: "postback", title: data['ayuda'], payload: data['ayuda']});
    }
    if (buttons.length > 0)
        elements.push({title: "Opciones de pregunta.", buttons});
    return {attachment: {type: "template", payload: {template_type: "generic", elements}}};
//        quick_replies.push({content_type: "text", title: data['ayuda'], payload: data['ayuda']});
//    return {text: 'Escoja una de las siguientes opciones.', quick_replies};
}