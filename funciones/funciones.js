
const config = require('../BBDD/clienteMysql');

/* global BASE_LOGICA, PAGINA, ID_TELEGRAM */

module.exports = {
    actualizarListaPaginas
};

function actualizarListaPaginas(callback) {
    config.ejecutarSQLCallback(SQL_ACTUALIZAR_PAGINAS, [], function (res) {
//        console.log("res",res,SQL_ACTUALIZAR_PAGINAS,ID_PROYECTO);
        if (!res || res['error'])
            return callback({en: -1, m: 'Algo ocurrio, intente mas tarde.', error: 'ERROR'});
        else if (res.length <= 0)
            return callback({en: 1, m: 'No hay paguinas que actualizar.'});
        else {
            for (var i = 0; i < res.length; i++) {
                var data = res[i];
                var dataJson = {};
                try {
                    dataJson = data['detalle'];
                } catch (e) {

                }
                if (dataJson['token'])
                    data['token'] = dataJson['token'];

//                console.log("data", data);
                PAGINA.set(data['idPlataforma'], data);
                if (data['idTipoPlataforma'] == ID_TELEGRAM) {
                    var telegram = require('../Telegram/coneccion');
                    if (!PAGINA_TELEGRAM.get(data['idTipoPlataforma']) && data['token']) {
                        PAGINA_TELEGRAM.set(data['idPlataforma'], telegram.coneccionTelegram(data['token'], data['idPlataforma']));
                    }
                }
            }
            return callback({en: 1, m: 'Paginas Actualizado.'});
        }
    });
}
var SQL_ACTUALIZAR_PAGINAS = "SELECT idPlataforma, nombre, idTipoPlataforma, clave, version, versionPlataforma, detalle, if(p.habilitado=1,1,0) as habilitado  FROM " + BASE_LOGICA + ".plataforma p ";
