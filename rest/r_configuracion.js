const router = require('express').Router();
const config = require('../BBDD/clienteMysql');
const fs = require('fs');
const boot = require('../bot/boot');
const modeloPreEntrenado = require('../entrenarModelo');
var entidad1 = require('../bot/entidades');
const funciones = require('../funciones/funciones');
const request = require('request');

router.post('/validarConversaciones', function (req, res) {
    var {text} = req.body;
    console.log("text", text, req.body);
    if (!text)
        text = 'hola';
    boot.validarMensaje("web", "web", text, 1, [], null, function (resValidador) {
        res.status(200).send(resValidador);
    });
});

router.post('/crearConversaciones', function (req, res) {
    var cormpus = require('../corpora/defecto.json');
    cormpus['sentences'] = [];
    res.status(200).send({en: 1, m: 'Se esta generando las entidades'});
    cargarIntencones(cormpus, function () {
        buscarEntidades(cormpus, function () {

        });
    });

});

router.post('/actualizarPlataforma', function (req, res) {
    funciones.actualizarListaPaginas(function (resActualizar) {
        res.status(200).send(resActualizar);
    });
});

router.post('/buscarSinonimo', function (req, res) {
    var {sinonimo} = req.body;
    if (!sinonimo)
        return res.status(200).send({en: -1, error: 'Error', param: 1, m: 'Faltan parámetros.'});
    else
        llamerServicio("http://sesat.fdi.ucm.es:8080/servicios/rest/sinonimos/json/" + sinonimo, function (respuesta) {
            if (respuesta['sinonimos'] && respuesta['sinonimos'].length > 0) {
                var data = [];
                for (var i = 0; i < respuesta['sinonimos'].length; i++) {
                    data.push({text: respuesta['sinonimos'][i]['sinonimo'], habilitado: 1, nuevo: true});
                }
                return res.status(200).send({en: 1, m: 'Datos encontrados.', data: data});
            } else
                return res.status(200).send({en: 1, m: 'No hay información.', data: []});
        });

});

router.post('/entrenarModelo', function (req, res) {
    var cormpus = require('../corpora/defecto.json');
    cormpus['sentences'] = [];
    res.status(200).send({en: 1, m: 'Se esta entrenando el modelo y entidades'});
    cargarIntencones(cormpus, function () {
//        console.log("Entrada 1");
        buscarEntidades(cormpus, function (respuesta) {
//             console.log("Entrada 2");
            entrenarModelo(respuesta['cormpus']);
        });
    });
});


function llamerServicio(url, callback) {
    var options = {method: 'GET', "rejectUnauthorized": false,
        url: url,
        headers: {version: '1.0.0', 'Content-Type': 'application/json'},
        json: true};
    request(options, function (error, response, body) {
        if (error) {
            callback({en: -1, error: "Error desconocido", m: "Algo ocurrio, intente más tarde"});
        } else
            return callback(response['body'] ? response['body'] : response);
    });
}

async function entrenarModelo(cormpus) {
//    modeloPreEntrenado.cargarCorpus('./corpora/dataUnl.json');
    MODELOCORPUS = await modeloPreEntrenado.entrenarModelo('./corpora/dataUnl.json', cormpus);
    entidad1.cargarEntidades();
    entidad1.cargarListaEntdades();
//    console.log("cormpus", (cormpus))
//    console.log("cormpus", MODELOCORPUS);
//    entidad1.cargarEntidades();
}

function cargarIntencones(corpus, callback) {
    config.ejecutarSQLCallback(SQL_OBTENER_INTENCIONES, [], function (respuestas) {
        if (!respuestas['error'] && respuestas.length > 0) {
            for (var i = 0; i < respuestas.length; i++) {
                var data = respuestas[i];
                data['text'] = data['text'].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                corpus['sentences'].push({text: data['text'], entities: data['entidades'], intent: data['intencion']});
            }
        }
        return callback({en: 1});
    });
}
var SQL_OBTENER_INTENCIONES = "SELECT i.nombre as intencion, e.text, e.entidades"
        + " FROM botUnl.entrenamientoIntencion e"
        + " inner join botUnl.intenciones i on e.idIntenciones=i.idIntenciones and e.habilitado=1 and i.idIntencionTipo=1 and i.habilitado=1"
        + " where e.habilitado=1";

function buscarEntidades(cormpus, callback) {
    config.ejecutarSQLCallback(SQL_BUSCAR_ENTIDADES, [], function (respuestas) {
        if (!respuestas['error'] && respuestas.length > 0) {
            for (var i = 0; i < respuestas.length; i++) {
                var data = respuestas[i];
                var info1 = "Información de " + data['entidadTres'] + " para " + data['entidadDos'];
                var info2 = data['entidadUno'] + " " + data['entidadDos'] + " " + data['entidadTres'];
                info1 = info1.toLowerCase();
                info2 = info2.toLowerCase();
                var eUno = data['entidadUno'].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                var eDos = data['entidadDos'].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                var eTres = data['entidadTres'].toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                if (cormpus['sentences'].filter(ent => (ent.text == eUno)).length == 0) {
                    cormpus['sentences'].push({text: eUno, entities: [], intent: "entidades"});
                    cormpus['sentences'].push({text: "Como me " + eUno, entities: [], intent: "entidades"});
                }
                if (cormpus['sentences'].filter(ent => (ent.text == eDos)).length == 0)
                    cormpus['sentences'].push({text: eDos, entities: [], intent: "entidades"});
                if (cormpus['sentences'].filter(ent => (ent.text == eTres)).length == 0)
                    cormpus['sentences'].push({text: eTres, entities: [], intent: "entidades"});

                cormpus['sentences'].push({text: info1.normalize("NFD").replace(/[\u0300-\u036f]/g, ""), entities: [{entidadDos: data['entidadDos'], entidadTres: data['entidadTres']}], intent: "entidades"});
                cormpus['sentences'].push({text: info2.normalize("NFD").replace(/[\u0300-\u036f]/g, ""), entities: [{entidadDos: data['entidadDos'], entidadTres: data['entidadTres']}], intent: "entidades"});
//                listaEntidad.push({text: "Información sobre " + data['entidadTres'] + " sobre " + data['entidadDos'], entities: [{entidadDos: data['entidadDos'], entidadTres: data['entidadTres']}], intent: "entidades"});
            }
            fs.writeFile('./corpora/dataUnl.json', JSON.stringify(cormpus), (err) => {
                if (err)
                    throw err;

                // success case, the file was saved
                console.log('Lyric saved!');
                callback({en: 1, cormpus});
            });
        } else
            callback({en: -1, cormpus});
    });
}

var SQL_BUSCAR_ENTIDADES = "select e.nombre as entidadUno,ee.nombre as entidadDos,eee.nombre as entidadTres"
        + " from botUnl.relacion r"
        + " inner join botUnl.entidad e on r.entidadUno=e.idEntidad"
        + " inner join botUnl.entidad ee on r.entidadDos=ee.idEntidad"
        + " inner join botUnl.entidad eee on r.entidadTres=eee.idEntidad where r.habilitado=1"
        + " union all "
        + " select ifnull(es1.nombre,e.nombre) as entidadUno,ifnull(es2.nombre,ee.nombre) as entidadDos,ifnull(es3.nombre,eee.nombre) as entidadTres"
        + " from botUnl.relacion r"
        + " inner join botUnl.entidad e on r.entidadUno=e.idEntidad"
        + " left join botUnl.entidadSinonimo es1 on es1.idEntidad=e.idEntidad and es1.habilitado=1"
        + " inner join botUnl.entidad ee on r.entidadDos=ee.idEntidad"
        + " left join botUnl.entidadSinonimo es2 on es2.idEntidad=ee.idEntidad and es2.habilitado=1"
        + " inner join botUnl.entidad eee on r.entidadTres=eee.idEntidad "
        + " left join botUnl.entidadSinonimo es3 on es3.idEntidad=eee.idEntidad and es3.habilitado=1"
        + " where r.habilitado=1";

module.exports = router;