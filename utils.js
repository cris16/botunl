
module.exports = {
    mapFields
};

function mapFields(input, fields) {
    if (!fields) {
        return input;
    }
    if (typeof fields === 'string') {
        return input.map(x => x[fields]);
    }
    return input.map(x => {
        const result = {};
        fields.forEach(field => {
            result[field] = x[field];
        });
        return result;
    });
}