/* global global, MENSAJE, process, CONFIGURACION */

global.MOMENT = require('moment-timezone');
global.IS_DESARROLLO = process.env.IS_DESARROLLO ? process.env.IS_DESARROLLO : 0;

//global.cormpus = null;
global.MODELOCORPUS = null;

global.BASE_LOGICA = 'botUnl';
global.MENSAJE = new Map();
global.PAGINA = new Map();
global.PAGINA_TELEGRAM = new Map();
global.ID_FACEBOK = 1;
global.ID_TELEGRAM = 2;
global.CONFIGURACION = new Map();

MENSAJE.set('INC', 'UPS esto es vergonzoso, por favor intenta de nuevo más tarde.');//Error Consulta
MENSAJE.set('NRDF', 'No encontramos la respuesta que solicitaste, ya le pasamos al adminsitrador de la cuenta tu inquietud.');//cUANDO EL MENAJE NO ESTA CONFIGURADO
MENSAJE.set('PRD', 'Esta es la información disponible');
MENSAJE.set('SRE', 'Lamento informarte que no pude encontrar la información que solicitaste recientemente. Te sugiero que hagas preguntas específicas sobre temas como matriculación, titulación, homologación y otros temas que puedan estar dentro de mi conocimiento. \nEstaré encantado de ayudarte en todo lo que pueda.');
MENSAJE.set('SPE', 'Tu pregunta es muy amplia y no encuentro una respuesta exacta para ti. Pero te puedo dar algunos consejos que te pueden ayudar.');
MENSAJE.set('UPS', 'Lamento mucho este inconveniente inesperado. Me doy cuenta de que esto puede ser frustrante para ti, pero por favor, no te preocupes, ya que puedo intentar ayudarte de otras maneras.\nSi te parece bien, puedo esperar a que verifiques lo sucedido y luego puedes volver a hacerme la pregunta. Estaré encantado de ayudarte en todo lo que pueda.\nRecuerda que estoy aquí para ayudarte en cualquier momento, así que no dudes en contactarme de nuevo si necesitas más ayuda en el futuro.');

CONFIGURACION.set('TtiempoEnvio',500);
CONFIGURACION.set('tiempoMensaje',500);
CONFIGURACION.set('tiempoEspera',500);