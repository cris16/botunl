/* global process */

require('dotenv').config();
require('./VAR_GLOBAL.js');


const request = require('request');
const express = require('express');
const modeloPreEntrenado = require('./entrenarModelo');
const funciones = require('./funciones/funciones');


const{urlencoded, json} = require('body-parser');
const app = express();

// Parse application/x-www-form-urlencoded
app.use(urlencoded({extended: true}));
app.use(require('./r_acces'));

// Parse application/json
app.use(json());

const directorio = process.env.DIRECTORIO ? process.env.DIRECTORIO : '/';
const PORT_SERVER = process.env.PORT_SERVER ? process.env.PORT_SERVER : '9095';

app.use(directorio + 'recurso/', require('./rest/r_configuracion'));
console.log("directorio", directorio);
if (process.env.USE_MESSENGER) {
    console.log("Facebook Activos");
    app.use(directorio + 'facebook/', require('./facebook/webhook'));
}

app.get(directorio, function (req, res) {
    res.status(200).send({EC: 'ECUADOR', by: 'CRIS', architect: 'cristophernagua@gmail.com', project: 'bot-UNL'});
});

app.listen(PORT_SERVER, function () {
    console.log("El servidor se encuentra el el puertoo", PORT_SERVER);

    prueba();
    funciones.actualizarListaPaginas(function () {

    });

});

async function prueba() {
//    modeloPreEntrenado.cargarCorpus('./corpora/dataUnl.json');
    MODELOCORPUS = await modeloPreEntrenado.entrenarModelo('./corpora/dataUnl.json');
}
