/* global MENSAJE */

var administrador = require('../logs/administradorLogs');

module.exports = {
    mensajeSimple,
    mensajeFormato,
    buscarTexto,
    mensajesMultiples,
    cargarAyuda
};

function mensajeSimple(mensaje) {
    return {text: mensaje};
}

function mensajesMultiples(data) {
    return data;
}

function mensajeFormato(datosMensaje) {
    var data = datosMensaje['data'];
//    console.log("data", data, datosMensaje);
    var respuestaDialogFlow = datosMensaje['respuestaDialogFlow'];
    let mensaje = {};
    try {
        mensaje = data[0]['respuesta'];
        if (mensaje['text'])
            mensaje['text'] = respuestaDialogFlow['respusta'];
        if (mensaje['attachment'] && mensaje['attachment']['payload'] && mensaje['attachment']['payload']['text'])
            mensaje['attachment']['payload']['text'] = respuestaDialogFlow['respusta'];
    } catch (e) {

    }
    return mensaje;
}

function buscarTexto(messagingEvent) {
    var respuesta = "dfgfdgrergdfgfhhdrsdfd";//Bote el mensaje de error
    if (messagingEvent.message.hasOwnProperty('text')) {
        if (messagingEvent.message.hasOwnProperty('quick_reply'))
            respuesta = messagingEvent.message.quick_reply.payload;
        else
            respuesta = messagingEvent.message.text;
    } else if (messagingEvent.message.attachments[0]['payload']['coordinates']) {
        respuesta = JSON.stringify(messagingEvent.message.attachments[0]['payload']['coordinates']);
    } else {
        administrador.error("ERROR, NO SE DETECTO ELEMENTO, POR DEFECTO SE LE ENVIA COMO ERROR:", JSON.stringify(messagingEvent));
    }
    return respuesta;
}

function cargarAyuda(respuestas) {
    var quick_replies = [], buttons = [], elements = [];
    for (var i = 0; i < respuestas.length; i++) {
        var data = respuestas[i];
        if (buttons.length == 3) {
            elements.push({title: "Alternativas de consulta", buttons});
            buttons = [];
        }
        buttons.push({type: "postback", title: data['ayuda'], payload: data['ayuda']});
    }
    if (buttons.length > 0)
        elements.push({title: "Opciones de pregunta.", buttons});
    return {attachment: {type: "template", payload: {template_type: "generic", elements}}};
//        quick_replies.push({content_type: "text", title: data['ayuda'], payload: data['ayuda']});
//    return {text: 'Escoja una de las siguientes opciones.', quick_replies};
}