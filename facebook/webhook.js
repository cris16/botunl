
/* global process, ID_FACEBOK, USARIOS_CONECTADOS, MENSAJE */

const router = require('express').Router();
const administrador = require('../logs/administradorLogs');
const request = require('request');
const boot = require('../bot/boot');
const utilsFacebook = require('./utils');
const facebook = require('./facebook');

module.exports = router;

router.get('/webhook', function (req, res) {
    if (req.query['hub.mode'] === 'subscribe' && req.query['hub.verify_token'] === process.env.FACEBOOK_TOKEN_VALIDACION) {
        administrador.informativo("Validating webhook");
        res.status(200).send(req.query['hub.challenge']);
    } else {
        administrador.error("ERROR: Fallo la validación del Token");
        res.sendStatus(403);
    }
});

router.post('/webhook', function (req, res) {
    let body = req.body;
    console.log("webhook", body);
    if (body.object === 'page') {

        // Iterates over each entry - there may be multiple if batched
        body.entry.forEach(function (pageEntry) {
            pageEntry.messaging.forEach(function (messagingEvent) {
                if (messagingEvent.message || messagingEvent.postback)
                    enviarMensaje(messagingEvent);

            });

        });

        res.status(200).send('EVENT_RECEIVED');
    } else {
        res.sendStatus(404);
    }
});

router.get('/authorize', function (req, res) {
    var accountLinkingToken = req.query['account_linking_token'];
    var redirectURI = req.query['redirect_uri'];
    var authCode = process.env.FACEBOOK_TOKEN_VALIDACION;
    var redirectURISuccess = redirectURI + "&authorization_code=" + authCode;
    res.render('authorize', {accountLinkingToken: accountLinkingToken, redirectURI: redirectURI, redirectURISuccess: redirectURISuccess});
});

function enviarMensaje(messagingEvent) {
    var mensajeMultiple = [];
    if (messagingEvent.postback) {
        if (messagingEvent['postback']['payload'] === "empezar")
            boot.validarMensaje(messagingEvent['recipient']['id'], messagingEvent['sender']['id'], "Hola", ID_FACEBOK, mensajeMultiple, null, function (respuesta) {
                return facebook.enviarMensajeMultiples(messagingEvent['recipient']['id'], messagingEvent['sender']['id'], mensajeMultiple, 0, respuesta['logs']);
            });
        else
            boot.validarMensaje(messagingEvent['recipient']['id'], messagingEvent['sender']['id'], messagingEvent['postback']['payload'], ID_FACEBOK, mensajeMultiple, null, function (respuesta) {
                return facebook.enviarMensajeMultiples(messagingEvent['recipient']['id'], messagingEvent['sender']['id'], mensajeMultiple, 0, respuesta['logs']);
            });
    } else if (messagingEvent.message && !messagingEvent.message.is_echo)
        if (messagingEvent.message.hasOwnProperty('text') || (messagingEvent.message.attachments && messagingEvent.message.attachments[0] && messagingEvent.message.attachments[0].type === 'location'))
            boot.validarMensaje(messagingEvent['recipient']['id'], messagingEvent['sender']['id'], utilsFacebook.buscarTexto(messagingEvent), ID_FACEBOK, mensajeMultiple, null, function (respuesta) {
                return facebook.enviarMensajeMultiples(messagingEvent['recipient']['id'], messagingEvent['sender']['id'], mensajeMultiple, 0, respuesta['logs']);
            });
        else
            administrador.error("mensaje no contemplado :" + messagingEvent['recipient']['id'] + " " + messagingEvent['sender']['id'], JSON.stringify(messagingEvent.message));
    else
        administrador.error("No se encontro em mensaje del usaurio :" + messagingEvent['sender']['id'], JSON.stringify(messagingEvent.message));
}
