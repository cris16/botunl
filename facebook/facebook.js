/* global PAGINA, CONFIGURACION */

const request = require('request');
const SERVER_URL_FACEBOOK = "https://graph.facebook.com/v";
const administrador = require('../logs/administradorLogs');

module.exports = {
//    enviarMensaje,
    obtenerDatosDeUsuario,
    enviarMensajeMultiples,
//    mensajesMasivos
};

function obtenerDatosDeUsuario(recipienteId, senderID, callback) {
    var dataPagina = PAGINA.get(recipienteId);
    if (dataPagina) {
        var PAGE_ACCESS_TOKEN = dataPagina['token'];
        request({url: SERVER_URL_FACEBOOK + dataPagina['versionPlataforma'] + "/" + senderID + "?fields=first_name,last_name,name&access_token=" + PAGE_ACCESS_TOKEN}, function (error, response, body) {
            if (!error)
                return callback({en: 1, datos: body, PAGE_ACCESS_TOKEN});
            return callback({en: -1, message: "No se pudo completar la operacion. Intente mas tarde"});
        });
    } else {
        administrador.error("No esta configurada La paguina obtenerDatosDeUsuario con el id:", recipienteId);
        return callback({en: -1, message: "No se pudo completar la operacion. Intente mas tarde"});
    }
}

function enviarMensajeMultiples(recipienteId, sessionId, data, pos, logs) {
    if (!logs)
        logs = {};
    if (data && data.length > pos) {
        setTimeout(function () {
            enviarMarcadoRecibido(recipienteId, sessionId);
            setTimeout(function () {
                enviarTipiando(recipienteId, sessionId, true);
                setTimeout(function () {
                    if (data) {
                        var message = {recipient: {id: sessionId}, message: data[pos]};
                        callSendAPIVarios(message, recipienteId, function (mensaje) {
                            if (mensaje['termino']) {
                                enviarTipiando(recipienteId, sessionId, false);
                                logs['mensaje_envio' + pos] = data[pos];
                                enviarMensajeMultiples(recipienteId, sessionId, data, pos + 1, logs);
                            }
                        });
                    }
                }, CONFIGURACION.get('TtiempoEnvio'));
            }, CONFIGURACION.get('tiempoMensaje'));
        }, CONFIGURACION.get('tiempoEspera'));
    } else if (data && pos == 0) {
        enviarMensaje(recipienteId, sessionId, data, logs);
    }
//    else if (logs)
//        administrador.guargarMongoLog(recipientId, 1, logs);
}
function enviarMensaje(recipienteId, sessionId, messagejson, logs) {
    if (messagejson)
        setTimeout(function () {
            enviarMarcadoRecibido(recipienteId, sessionId);
            setTimeout(function () {
                enviarTipiando(recipienteId, sessionId, true);
                setTimeout(function () {
                    callSendAPI({recipient: {id: sessionId}, message: messagejson}, recipienteId);
                    enviarTipiando(recipienteId, sessionId, false);
                }, CONFIGURACION.get('TtiempoEnvio'));
            }, CONFIGURACION.get('tiempoMensaje'));
        }, CONFIGURACION.get('tiempoEspera'));
}

function callSendAPIVarios(messageData, recipienteId, callback) {
    var dataPagina = PAGINA.get(recipienteId);
//    console.log("dataPagina", dataPagina, messageData);
    if (dataPagina) {
        var PAGE_ACCESS_TOKEN = dataPagina['token'];
        request({method: 'POST', uri: SERVER_URL_FACEBOOK + dataPagina['versionPlataforma'] + '/me/messages', qs: {access_token: PAGE_ACCESS_TOKEN}, json: messageData}, function (error, response) {
//            console.log("error", error, response);
            if (error)
                administrador.error("callSendAPIVarios", error);
            return callback({termino: true});
        });
    } else {
        administrador.error("No esta configurada La paguina callSendAPIVarios con el id:" + recipienteId);
        return callback({termino: true});
    }
}

function enviarMarcadoRecibido(recipienteId, sessionId) {
    var messageData = {recipient: {id: sessionId}, sender_action: "mark_seen"};
    callSendAPI(messageData, recipienteId);
}

function enviarTipiando(recipienteId, sessionId, isTyping) {
    var messageData = {recipient: {id: sessionId}, sender_action: isTyping ? "typing_on" : "typing_off"};
    callSendAPI(messageData, recipienteId);
}

function callSendAPI(messageData, recipienteId) {
    var dataPagina = PAGINA.get(recipienteId);
//    console.log("EnviarMensaje", messageData, dataPagina);
    if (dataPagina) {
        var PAGE_ACCESS_TOKEN = dataPagina['token'];
        request({method: 'POST', uri: SERVER_URL_FACEBOOK + dataPagina['versionPlataforma'] + '/me/messages', qs: {access_token: PAGE_ACCESS_TOKEN}, json: messageData}, function (error) {
            if (error)
                administrador.error("callSendAPI", error);
        });
    } else {
        administrador.error("No esta configurada La paguina callSendAPI con el id:" + recipienteId);
    }

}