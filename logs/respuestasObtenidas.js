/* global MOMENT */

const config = require('../BBDD/clienteMysql');

module.exports = {
    guardarLog
};

function guardarLog(idPlataforma, idCliente, mensajeEntrada, respuesta, porcentajeProximacion, intencion, infoUser) {
//    if (infoUser)
    crearCliente(idCliente, idPlataforma, infoUser);
    let fechaReg = MOMENT().tz('America/Guayaquil');
    config.ejecutarSQLCallback(SQL_GUARDAR_LOG, [idPlataforma, idCliente, mensajeEntrada, respuesta, porcentajeProximacion, intencion, fechaReg.format('YYYY-MM-DD'), fechaReg.format('hh:mm:ss')], function (respuestas) {

    });
}
var SQL_GUARDAR_LOG = "INSERT IGNORE INTO botUnl.logs (idPlataforma, idCliente, mensajeEntrada, respuesta, porcentajeProximacion, idIntencion, fecha, hora) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

function crearCliente(idCliente, idPlataforma, dataCliente) {
    var nombre = null, apellido = null, alias = null;
    if (dataCliente)
        try {//{"id": 503974730, "is_bot": false, "last_name": "Nagua Rivas", "first_name": "Cristopher Daniel", "language_code": "es"}
            dataCliente = JSON.parse(dataCliente);
            nombre = dataCliente['first_name'];
            apellido = dataCliente['last_name'];
            alias = nombre + " " + (apellido ? apellido : "");
        } catch (error) {

        }
    config.ejecutarSQLCallback(SQL_CREAR_CLIENTES, [idPlataforma, idCliente, nombre, apellido, alias], function (respuestas) {

    });
}

var SQL_CREAR_CLIENTES = "INSERT INTO botUnl.clientes (idPlataforma, idClientes, nombre, apellido, alias) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE fechaUltimaConeccion=now()";