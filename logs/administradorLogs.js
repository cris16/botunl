

/* global MOMENT */

module.exports = {
    log,
    error,
    informativo
};

function log(log, extra) {
    if (!extra)
        extra = {};
    console.log('LOG', MOMENT().tz('America/Guayaquil').format('YYYY-MM-DD, hh:mm:ss'), log, extra);
}

function error(log, extra) {
    if (!extra)
        extra = {};
    console.error('ERROR_LOG:', MOMENT().tz('America/Guayaquil').format('YYYY-MM-DD, hh:mm:ss'), log, extra);
}
function informativo(log, extra) {
    if (!extra)
        extra = {};
    console.info('INFORMATIVO_LOG:', MOMENT().tz('America/Guayaquil').format('YYYY-MM-DD, hh:mm:ss'), log, extra);
}