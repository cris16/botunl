const {PorterStemmer} = require("natural");
const {Corpora, featuresToDict, chain} = require("evaluate-nlp");
const {NeuralNetwork} = require("brain.js");
//
const utils = require('./utils');

//var modelo = null;

module.exports = {
    cargarCorpus,
    entrenarModelo,
    realizarPrueba,
    realizarPrueba2

};

async function cargarCorpus(url) {
    cormpus = require(url);
//    MODELOCORPUS = await entrenarModelo(url);
//    console.log("DataCargada", cormpus);
}

async function entrenarModelo(url, data) {
//    var crp = cormpus;
//    if (url)
    var crp = require(url);
    if (data)
        crp = data;
    console.log("Ingreso a entrenar Modelo", url);
    var model = nlpEngine.createNetwork();
    const trainXs = utils.mapFields(crp.sentences, 'text');
    const trainYs = utils.mapFields(crp.sentences, 'intent');
    const trainZs = utils.mapFields(crp.sentences, 'entities');
    await nlpEngine.train(model, trainXs, trainYs, trainZs);
    console.log("Modelo Entrenado", url);
    return model;
}

function realizarPrueba(text, model) {
    return {text, result: ordenarResulatdo(nlpEngine.predict(model, text))};
//    return nlpEngine.predict(modelo, text);
}
function realizarPrueba2(text, model) {
    return {text, result: (nlpEngine.predict2(model, text))};
//    return nlpEngine.predict(modelo, text);
}

function augment(x, entities) {
    const result = [x];
    let augmented = x;
    entities.forEach(entity => {
        augmented = augmented.replace(entity.text, `%${entity.entity}%`);
    });
    if (augmented !== x) {
        result.push(augmented);
    }
    return result;
}

const tokenize = str => str.split(/\W+/);
const trim = arr => arr.filter(x => x !== "");
const lowerize = arr => arr.map(x => x.toLowerCase());
const stem = arr => arr.map(x => PorterStemmer.stem(x));
const utteranceToText = str =>
    chain(str, tokenize, trim, lowerize, stem, featuresToDict);

const nlpEngine = {
    createNetwork: () =>
        new NeuralNetwork({
            hiddenLayers: [],
            activation: "leaky-relu",
            errorThresh: 0.00005,
            learningRate: 0.1
        }),
    train: (net, xs, ys, zs) => {
        const input = [];
        for (let i = 0; i < xs.length; i += 1) {
            const augmented = augment(xs[i], zs[i]);
            augmented.forEach(x => {
                input.push({input: utteranceToText(x), output: {[ys[i]]: 1}});
            });
//            console.log("input", input);
        }
        return net.train(input);
    },
    predict: (net, x) => net.run(utteranceToText(x)),
    predict2: (net, x) => net.run((x))
};


function ordenarResulatdo(data) {
    var respuesta = [];
    Object.keys(data).forEach(function (key) {
        respuesta.push({intens: key, aproximado: data[key]});
    });
    return respuesta.sort((a, b) => b.aproximado - a.aproximado);
//    console.log(respuesta.sort((a,b)=>b.aproximado-a.aproximado));
}